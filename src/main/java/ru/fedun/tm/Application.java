package ru.fedun.tm;

import java.util.Scanner;

import static ru.fedun.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        if (args.length != 0) {
            run(args[0]);
        }
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    private static void run(final String command) {
        if (command == null || command.isEmpty()) {
            System.out.println("Empty command");
            return;
        }
        switch (command) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_EXIT:
                exit();
                break;
            default:
                displayError();
        }
    }

    private static void displayWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Shutdown program.");
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Alexander Fedun");
        System.out.println("sasha171998@gmail.com");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayError() {
        System.out.println("Error: unknown command");
    }

}
